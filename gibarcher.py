#!/usr/bin/env python
import sys, re, argparse, json
from base64 import b64decode
import requests

def main(url):
    h = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0'}
    site = 'https://www.watchcartoononline.io'
    r = requests.get(url, headers=h)

    try:
        scriptblob = re.search(r'<script>(.*?)</script>', r.text).group(1)
    except Exception as e:
        raise ValueError("Dafuq is dis: %s") % e.message

    b64s = re.findall(r'"([A-z0-9+/=]+)"', scriptblob)
    n = int(re.search(r' - ([0-9]+)\)', scriptblob).group(1))

    decode = ""
    for x in b64s:
        raw = str(b64decode(x), 'utf-8')
        decode += chr(int(re.sub(r'\D', '', raw)) - n)

    frame = re.search(r'src="(.*?)"', decode).group(1)
    print("Getting %s%s" % (site, frame), file=sys.stderr)
    r = requests.get('%s%s' % (site, frame), headers=h)
    link = re.search(r'source src="(.*?)"', r.text)
    if link is None:
        # Need to get a tricksy vid lik
        vidlink = re.search(r'getvidlink.php\?v=(.*?)"', r.text).group(1)
        print(f"VidLink: {vidlink}", file=sys.stderr)

        h['X-Requested-With'] = 'XMLHttpRequest'
        r = requests.get(f'{site}/inc/embed/getvidlink.php?v={vidlink}', headers=h)
        print(f"Have raw: {r.text}", file=sys.stderr)
        jdata = json.loads(r.text)
        return f"{jdata['server']}/getvid?evid={jdata['hd']}"
    else:
        return link.group(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''
    Outputs true source URL for archer episodes from watchcartoononline.io
    ''',
    epilog='''
    Use alongside wget and possibly a while loop to download seasons such as:
    wget 'https://www.watchcartoononline.io/anime/archer/' -q -O - |
            grep archer-season-6 |
            perl -nle '/href="(.*?)"\s/; print $1' |
            while read line; do ./gibarcher.py $line | wget -i -; done
    ''')
    parser.add_argument('url', metavar='url', type=str,
            help='A URL with full path to archer episode')
    args = parser.parse_args()

    print(main(args.url))
