#!/usr/bin/perl
use strict;
use warnings;

my $DIR = $ENV{DIR} || '/var/cache/pacman/pkg/';
chdir $DIR or die "Couldn't move to cache? ($!)$/";
my %jar;
my ($bytes, $num);
$bytes = $num = 0;

sub _say;

_say("Checking $DIR");

for (<"*.tar.zst">) {
    /^(.*?)-(\d.*?)\.pkg/;
    unless ($1 && $2) {
        _say("$_ seems to be garbage in the cache dir?");
        next;
    }

    $jar{$1} = [] if (!exists($jar{$1}));
    push @{$jar{$1}}, $2;
}

# Sort the things
for my $p (keys %jar) {
    my @a = @{$jar{$p}};
    my @d = splice(@a, 0, (@a - 2));
    next unless @d;
    print "\nDeleting " . @d . " $p packages:\n";
    print "\t$_\n" for (@d);
    my @delete = map { "$p-$_.pkg.tar.zst" } @d;
    # count bytes
    $bytes += (stat($_))[7] and $num++ for (@delete);
    unlink @delete or _say("Failed to delete: $!$/");
}

# Give some stats
_say("$/== $bytes bytes in $num files ==");

sub _say {
    warn "@_", $/;
}
