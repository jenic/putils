#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

# Based on http://crdx.org/misc/cookies/ (but with bugfixes)
# Given cookies in document.cookie format, converts to Netscape cookies.txt
# format, suitable for tools such as wget.
# Example:
# ./mkcookies.pl --domain example.org \
#       --cookies 'remember_me=true; APISID=bG9sCg==; _U="u=1234"'
# Output:
# .example.com  TRUE    /   FALSE   1234567 remember_me true
# .example.com  TRUE    /   FALSE   1234567 APISID  bG9sCg==
# .example.com  TRUE    /   FALSE   1234567 _U  "u=1234"

use constant _1DAY => 86400;
my ($domain, $raw, $help, $man);

GetOptions
( 'domain=s' => \$domain
, 'cookies=s' => \$raw
, 'help|?' => \$help
, 'man' => \$man
) or pod2usage(2);

pod2usage(-exitval => 0, -verbose => 2) if $man;
pod2usage(1) if $help || !$domain || !$raw;

for (split(/;\s/, $raw)) {
    /^(.*?)=(.*?)$/;
    printf ".%s\tTRUE\t/\tFALSE\t%i\t%s\t%s\n",
        $domain,
        time + _1DAY,
        $1,
        $2;
}

__END__

=head1 mkcookies

Perl utility to convert document.cookie to Netscape cookies.txt format

=head1 SYNOPSIS

mkcookies.pl --domain <domain> --cookies <document.cookie>

EX: mkcookies.pl --domain example.com --cookies 'APISID=bG9sCg==; _U="u=1234"'

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exit

=item B<-man>

Prints the manual page and exits

=item B<-domain>

B<Required.> Specify domain name field.

=item B<-cookies>

B<Required.> Specify cookies string in document.cookie format

=back

=head1 DESCRIPTION

B<mkcookies> is based on http://crdx.org/misc/cookies/ but has been rewritten
as a Perl script, with minor fixes to how it handles input. Original script did
not properly handle cookies such as id="u=123"
